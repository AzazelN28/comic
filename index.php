<?php

/**
 * index.php
 *
 *	OpenComic
 *
 * @author	Aitor Moreno Melcón
 * @version	1.0
 */

/**
 * Funciones que se usarán en las plantillas.
 *
 *
 */
function title(){
	echo 'Titulo';
}

define('BASE_PATH',__DIR__);
define('BASE_URL',str_replace($_SERVER['DOCUMENT_ROOT'],'',BASE_PATH));

$pages = array();
$path = __DIR__ . DIRECTORY_SEPARATOR . 'pages'; 
if (($handler = opendir($path)) !== false) {
	while($file = readdir($handler)) {
		if ($file != '.' && $file != '..') {
			$extension = pathinfo($file,PATHINFO_EXTENSION);	
			$filepath = $path . DIRECTORY_SEPARATOR . $file;
			if (in_array($extension,array('jpg','jpeg','jpe','png','gif'))) {
				$pages[] = 'pages/' . $file;
			}
		}
	}
	sort($pages);
	closedir($handler);
}

$metadata = json_decode(file_get_contents('metadata.json'),true);

$url = str_replace(BASE_URL,'',$_SERVER['REQUEST_URI']);
if (preg_match('/^\/?$/',$url)) {

    require_once __DIR__ . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'index.phtml';

} else if (preg_match('/^\/?(?P<page>[0-9]+)$/',$url,$matches)) {

	require_once __DIR__ . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'index.phtml';

} else if (preg_match('/^\/?about$/',$url,$matches)) {

	require_once __DIR__ . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'about.phtml';
	
} else {

	require_once __DIR__ . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'notfound.phtml';

}

?>

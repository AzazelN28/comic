// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating

// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel

// MIT license

(function() {
	var lastTime = 0;
	var vendors = ['ms', 'moz', 'webkit', 'o'];
	for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
		window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
		window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
		|| window[vendors[x]+'CancelRequestAnimationFrame'];
	}

	if (!window.requestAnimationFrame)
		window.requestAnimationFrame = function(callback, element) {
			var currTime = new Date().getTime();
			var timeToCall = Math.max(0, 16 - (currTime - lastTime));
			var id = window.setTimeout(function() { callback(currTime + timeToCall); },
			timeToCall);
			lastTime = currTime + timeToCall;
			return id;
		};

	if (!window.cancelAnimationFrame)
		window.cancelAnimationFrame = function(id) {
			clearTimeout(id);
		};
}());

(function(){

	var Comic = (function(){

		function __class() {

			var page = undefined,
					pages = undefined,
					previousPage = undefined,
					pagesContainer = document.querySelector('.pages'),
					nextLink = document.querySelector('a[rel="next"]'),
					previousLink = document.querySelector('a[rel="previous"]'),
					firstLink = document.querySelector('a[rel="first"]'),
					lastLink = document.querySelector('a[rel="last"]'),
					info = document.querySelector('.info'),
					preferredOrientation = null,
					minimumX = 0.2,
					animationRequest = null,
					move = {
						initX : 0,
						endX : 0,
						currentX : 0,
						isDown : false
					};

			if (pagesContainer.hasAttribute('data-orientation')) {
				preferredOrientation = pagesContainer.getAttribute('data-orientation');
				if (preferredOrientation == 'landscape' || preferredOrientation == 'portrait') {
					info.className = 'info hidden';
				} else {
					preferredOrientation = null;
					info.className = 'info hidden';
				}
			} else {
				info.className = 'info hidden';
			}

			function updateNavigation() {
				if (page == 0) {
					previousLink.className = 'navigation hidden';
					nextLink.className = 'navigation';
				} else if (page == pages - 1) {
				    previousLink.className = 'navigation';
					nextLink.className = 'navigation hidden';
				} else {
					previousLink.className = nextLink.className = 'navigation';
				}
			}

			function getPageFromUrl() {
				var matches = /\/?([0-9]+)$/.exec(location.href);
				if (matches) {
					page = parseInt(matches[1]);
				} else {
					if (window.localStorage) {
						page = window.localStorage.getItem('page');
						if (page === null) {
							page = 0;
						} else {
							var matches = /^https?:\/\/(.*)\//i.exec(location.href);
							var url = matches[0];
							if (window.history) {
								window.history.pushState({ page: page},document.title,url + page);
							}
						}
					} else {
						page = 0;
					}
				}

				updateNavigation();
				go();
			}

			if (page === undefined) {
				getPageFromUrl();
			}

			if (pages === undefined) {
				pages = pagesContainer.children.length;
			}

			if (window.history) {
				window.addEventListener('popstate',function(e) {
					e.preventDefault();
					if (e.state && e.state.page !== undefined) {
						page = e.state.page;
						animate();
						updateNavigation();
					} else {
						getPageFromUrl();
					}
				});
			}

			function loadImagesFrom(index,min){
				var lis = document.querySelectorAll('.pages > li');
				if (index < 0) {
					index = 0;
				}
				var total = (pages - index);
				min = (min > total ? total : min);
				var loaded = 0;
				i = index;
				while(i < total) {
					var img = new Image();
					img['data-container'] = lis[i];
					img['data-index'] = i;
					img.src = lis[i].getAttribute('data-url');
					img.addEventListener('load',function(e){

						var container = this['data-container'];
						container.appendChild(this);

						loaded++;
						if (loaded >= min) {

							var overlay = document.querySelector('.overlay');
							overlay.className = 'overlay hidden';
							updateNavigation();

						}
					});
					i++;
				}

			}

			// este método carga cómo mínimo imágenes desde la posición actual.
			loadImagesFrom(page - 5,5);

			function interpolate(a,b,p) {
				return (a + ((b - a) * p));
			}

			function translateX(element,x) {
				var style = 'translateX(' + x + 'px)';
				element.style.msTransform = element.style.MozTransform = element.style.OTransform = element.style.webkitTransform = style;
			}

			function go() {
				var x = -page * width();
				translateX(pagesContainer, x);
				updateNavigation();
			}

			function animate() {
				var startTime = new Date().getTime(),
						currentTime = new Date().getTime(),
						duration = 300;

				if (animationRequest != null) {
					window.cancelAnimationFrame(animationRequest);
				}

				(function fx(){
					currentTime = new Date().getTime();

					var time = currentTime - startTime;
					if (time >= duration) {
						time = duration;
					}

					var progress = (time / duration) ;
					var x = move.currentX = interpolate(move.currentX, -page * width(), progress);
					translateX(pagesContainer, x);

					if (time < duration){
						animationRequest = window.requestAnimationFrame(fx);
					}

				})();

			}

			function previous() {
				if (page > 0) {
					previousPage = page;
					page--;
					update();
				}
			}

			function next() {
				if (page < (pages - 1)) {
					previousPage = page;
					page++;
					update();
				}
			}

			function first() {
				if (page != 0) {
					previousPage = page;
					page = 0;
					update();
				}
			}

			function last() {
				if (page != (pages - 1)) {
					previousPage = page;
					page = (pages - 1);
					update();
				}
			}

			function width() {
				return (window.innerWidth || document.body.offsetWidth || document.documentElement.offsetWidth);
			}

			function update() {

				var matches = /^https?:\/\/(.*)\//i.exec(location.href);
				var url = matches[0];

				updateNavigation();

				// este método actualiza el
				if (window.history) {

					// añadimos el estado al historial.
					window.history.pushState({ page: page }, document.title, url + page);

					// almacenamos la página en el localStorage.
					window.localStorage.setItem('page',page);

					// realizamos la animación.
					animate();

				} else {

					location.href = url + page;

				}

			}

			function firstHandler(e){
				e.preventDefault();
				first();
			}

			function lastHandler(e){
				e.preventDefault();
				last();
			}

			function nextHandler(e){
				e.preventDefault();
				next();
			}

			function previousHandler(e){
				e.preventDefault();
				previous();
			}

			function downHandler(e){
				if (!e.changedTouches) {
					if (e.button != 0) {
						return;
					}
				}
				move.initX = (e.changedTouches ? e.changedTouches[0].clientX : e.clientX);
				move.isDown = true;
				e.preventDefault();
			}

			function upHandler(e){
				if (!e.changedTouches) {
					if (e.button != 0) {
						return;
					}
				}

				move.endX = (e.changedTouches ? e.changedTouches[0].clientX : e.clientX);
				var x = (move.endX - move.initX);
				move.currentX = (-page * width()) + x;
				var percentage = (x / width());
				if (percentage < -minimumX) {
					next();
				} else if (percentage > minimumX) {
					previous();
				}
				animate();
				move.isDown = false;
				e.preventDefault();
			}

			function moveHandler(e){
				if (move.isDown === true) {
					var x = move.currentX = (-page * width()) + ((e.changedTouches ? e.changedTouches[0].clientX : e.clientX) - move.initX);
					translateX(pagesContainer, x);
					e.preventDefault();
				}
			}

			function keyDownHandler(e){
				var which = e.which || e.keyCode;
				if (which == 37 || which == 39)
					e.preventDefault();
			}

			function keyUpHandler(e){
				var which = e.which || e.keyCode;
				if (which == 37) {
					previousLink.click();
					e.preventDefault();
				} else if (which == 39) {
					nextLink.click();
					e.preventDefault();
				}
			}

			function resizeHandler(e){
				go();
			}

			function orientationHandler(e){
				var width = window.innerWidth || document.body.offsetWidth || document.documentElement.offsetWidth,
						height = window.innerHeight || document.body.offsetHeight || document.documentElement.offsetHeight;

				if (width > height) {
					if (preferredOrientation != null && preferredOrientation != 'landscape') {
						info.className = 'info';
					} else {
						info.className = 'info hidden';
					}
				} else {
					if (preferredOrientation != null && preferredOrientation != 'portrait') {
						info.className = 'info';
					} else {
						info.className = 'info hidden';
					}
				}
			}

			if (!nextLink) {
				throw 'El enlace de página siguiente no está definido';
			}

			if (!previousLink) {
				throw 'El enlace de página anterior no está definido';
			}

			nextLink.addEventListener('click',nextHandler);
			previousLink.addEventListener('click',previousHandler);

			if (firstLink) {
				firstLink.addEventListener('click',firstHandler);
			}

			if (lastLink) {
				lastLink.addEventListener('click',lastHandler);
			}

			pagesContainer.addEventListener('mousedown',downHandler);
			pagesContainer.addEventListener('touchstart',downHandler);

			pagesContainer.addEventListener('mousemove',moveHandler);
			pagesContainer.addEventListener('touchmove',moveHandler);

			pagesContainer.addEventListener('mouseup',upHandler);
			pagesContainer.addEventListener('touchend',upHandler);
			pagesContainer.addEventListener('touchcancel',upHandler);

			window.addEventListener('resize',resizeHandler);

			// evento encargado de gestionar las pulsaciones
			// de teclado.
			document.addEventListener('keydown',keyDownHandler);
			document.addEventListener('keyup',keyUpHandler);

			// inhabilitamos el click derecho.
			/*document.addEventListener('contextmenu',function(e){
				e.preventDefault();
			});*/

			if (preferredOrientation != null) {
				window.addEventListener('deviceorientation',orientationHandler);
			}

		}

		return __class;

	})();

	new Comic();

})();

